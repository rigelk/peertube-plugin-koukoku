async function register({ registerSetting }) {
  // example tags: https://developers.google.com/interactive-media-ads/docs/sdks/html5/tags
  const adTagUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator="

  registerSetting({
    name: "ad-tag-url",
    label: "URL which returns a VAST, VMAP or ad rules, response",
    type: "input",
    private: false,
    default: adTagUrl
  })

  registerSetting({
    name: "ad-skippable-after-seconds",
    label: "Number of seconds after which an ad can be skipped ('0' to disable)",
    type: "input",
    private: false,
    default: "5"
  })
}

async function unregister() {
  return
}

module.exports = {
  register,
  unregister
}
