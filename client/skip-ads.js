var extend = function(obj) {
  var arg, i, k
  for (i = 1; i < arguments.length; i++) {
    arg = arguments[i]
    for (k in arg) {
      if (arg.hasOwnProperty(k)) {
        obj[k] = arg[k]
      }
    }
  }
  return obj
}

var SkipAds = function(player, options) {
  this.player = player
  this.settings = extend(this.defaults(), options || {})
  this.init()
}

SkipAds.prototype.hasClass = function(el, className) {
  return el.className && !!el.className.match("(^|\\s)" + className + "($|\\s)")
}

SkipAds.prototype.removeClass = function(el, className) {
  el.className = el.className.replace(
    new RegExp("(^|\\s)" + className + "($|\\s)", "g"),
    " "
  )
}

SkipAds.prototype.addClass = function(el, className) {
  if (!this.hasClass(el, className)) {
    el.className += " " + className
  }
}

SkipAds.prototype.translate = function(string, code = undefined) {
  if (code && this.settings.strings.hasOwnProperty(code)) {
    const translation = this.settings.strings[code]
    if (translation !== code) return translation
  }
  return string
}

SkipAds.prototype.defaults = function() {
  /**
   * default parameters for the plugin.
   *
   * Note that this plugin doesn't take into account the
   * skippable status of the VAST response, nor its skip
   * offset (that we call delayInSeconds in this plugin)
   */
  return {
    skip: true,
    delayInSeconds: 5,
    strings: {}
  }
}

SkipAds.prototype.init = function() {
  this.createButton()
  this.setupEventHandlers()
}

SkipAds.prototype.createButton = function() {
  var adsInfo = document.createElement("div")
  adsInfo.className = "videojs-ads-info"

  var timeLeftInfo = document.createElement("span")
  timeLeftInfo.innerHTML = ""
  adsInfo.appendChild(timeLeftInfo)

  if (this.settings.skip) {
    var skipButton = document.createElement("a")
    skipButton.innerHTML = this.translate("Skip Ad", "skip-ad")
    skipButton.onclick = this.skipButtonClicked.bind(this)
    adsInfo.appendChild(skipButton)
    this.skipButton = skipButton
  }

  this.player.el().appendChild(adsInfo)
  this.adsInfo = adsInfo
  this.timeLeftInfo = timeLeftInfo
}

SkipAds.prototype.skipButtonClicked = function(event) {
  this.player.ima.controller.onAdBreakEnd()
}

SkipAds.prototype.adTimeUpdate = function(event) {
  var player = this.player

  let adDuration
  try {
    adDuration = player.ima.getAdsManager().getCurrentAd().getDuration()
  } catch (e) {
    return
  }

  /**
   * adtimeupdate should be triggered every second,
   * but doesn't for some reason. We take it from
   * there and poll until enough seconds have passed
   * to show the skip button.
   */
  const toggleSkip = setInterval(() => {
    if (player.ads.isInAdMode()) {
      // time left before video
      var timeLeft = Math.round(player.ima.getAdsManager().getRemainingTime())
      var elapsedTime = adDuration - timeLeft
      var timeLeftBeforeSkippable = Math.max(this.settings.delayInSeconds - elapsedTime, 0)

      if (this.settings.skip && timeLeft !== this.timeLeft) {
        this.addClass(this.adsInfo, "enabled")
        this.timeLeftInfo.style.display = "inherit"
        this.timeLeft = timeLeft
        this.timeLeftInfo.innerHTML =
          `${this.translate("Skip to video in", "skip-to-videos-in")} ${timeLeftBeforeSkippable}`
      }

      // we toggle 500ms before the actual delay ends, to prevent showing an ugly "(Skip to video in 0)"
      if (this.settings.skip && elapsedTime > this.settings.delayInSeconds - 0.5) {
        this.timeLeftInfo.style.display = "none"
        this.skipButton.style.display = "inherit"
      }
    } else {
      clearInterval(toggleSkip)
    }
  }, 500)
}

SkipAds.prototype.adEnd = function(event) {
  this.removeClass(this.adsInfo, "enabled")
}

SkipAds.prototype.setupEventHandlers = function() {
  this.player.on("adend", this.adEnd.bind(this))
  this.player.on("adtimeupdate", this.adTimeUpdate.bind(this))
}

const init = function(options) {
  if (this.ads === undefined) {
    console.error("This plugin requires videojs-contrib-ads")
    return null
  }

  this.skipAds = new SkipAds(player, options)
}

module.exports = init
