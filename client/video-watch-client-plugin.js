const { loadImaSdk } = require("@alugha/ima")

function register({ registerHook, peertubeHelpers }) {
  if (isDnt()) return
  loadImaSdk()

  initKoukoku(registerHook, peertubeHelpers)
    .catch(err => console.error('Cannot initialize koukoku plugin', err))
}

export { register }

function initKoukoku (registerHook, peertubeHelpers) {
  return peertubeHelpers.getSettings()
    .then(s => {
      if (!s || !s['ad-tag-url']) {
        console.error('IMA plugin lacks a proper VAST-compliant URL set to function.')
        return
      }

      const adTagUrl = s["ad-tag-url"]
      const delayInSeconds = parseInt(s["ad-skippable-after-seconds"], 10)

      registerHook({
        target: "action:video-watch.video.loaded",
        handler: params => {
          let videojs = window.videojs = params.videojs
          // init contrib-ads
          const contribAdsPlugin = window.contribAdsPlugin = require('videojs-contrib-ads').default
          params.videojs.registerPlugin('ads', contribAdsPlugin)

          // init ima
          let ima = require('videojs-ima/dist/videojs.ima')
          videojs.registerPlugin(ima.name, ima.init)

          // init skip ads button
          const skipAds = require('./skip-ads')
          videojs.registerPlugin('skipAds', skipAds)
        }
      })

      registerHook({
        target: "action:video-watch.player.loaded",
        handler: async params => {
          window.player = params.player
          const options = {
            disableFlashAds: true,
            adTagUrl
          }
          params.player.ima(options)
          params.player.skipAds({
            skip: delayInSeconds !== 0,
            delayInSeconds,
            strings: {
              'skip-to-videos-in': await peertubeHelpers.translate('skip-to-videos-in'),
              'skip-ad': await peertubeHelpers.translate('skip-ad')
            }
          })
        }
      })
    })
}

let isDnt = () => {
  // get the value from the various browser implementations.
  let dnt_active = parseInt(
    // Internet Explorer 9 and 10 vendor prefix
    navigator.msDoNotTrack ||

    // IE 11 uses window.doNotTrack
    window.doNotTrack ||

    // W3C
    navigator.doNotTrack,
    10
  );

  // If this comes to exactly 1 DNT is set.
  return (dnt_active === 1)
}
