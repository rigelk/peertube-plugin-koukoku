# 広告 - Kōkoku

Advertisements on your PeerTube instance with any VAST-compliant server, mindful of DNT.

## Read First

This plugin is NOT supported by Framasoft, nor any organisation.

notes:
- this plugin doesn't track watchers, but the ad server could (more often than not, they do). Be mindful of your choice.
- only the video watch page is affected. embeds are not. raw files are not.
- since PeerTube only supports admin settings for plugins, for now all settings fall upon the admin:
  - the admin sets the ad server. it can be anything as long as the URL responds a valid VAST payload.
  - the admin gets the potential revenue of the ads: there is no re-distribution system to the creators.
  - the admin sets how many videos you get to see before a new video gets ads on it.
  - the admin sets if one can skip ads, and if yes how long after it started.
- Do Not Track (DNT) is respected, and nothing is loaded in its presence.

notes of importance:
- by using a third-party ad server, you are responsible for notifying your community of it
- update the terms of service of your PeerTube instance to reflect the ad server use
- ad servers are more often than not tracking those watching their ads. Be mindful of your choice. Use your own server if possible.

![](http://lutim.cpy.re/EUOeuZp8.png)

## What technologies are used?

The IMA SDK is used. It doesn't provide ads: it is just making the video player able to load videos from an ad server.

## Why use the IMA SDK?

>With the inclusion of certain blocks of code and dependencies in a publisher’s video player declaration, the IMA integrates into the player and supplies APIs to handle operations and events related to video ads.
>
>Simply put, the SDK makes a video player capable of parsing video ad requests, interpreting responses from ad servers,and generally carrying out the operations needed to successfully serve an ad.
>
>IMA works with the VAST, VMAP, and VPAID video ad serving standards maintained by the IAB. This means that aside from working with Google-run services like DoubleClick for Publishers and the AdSense network, the IMA can also pull in ads from any third-party ad server as long as it’s VAST-compliant.

Just choose your own VAST-compliant server, set it in the parameters of the plugin and you will be set.
