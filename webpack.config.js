const webpack = require("webpack")
const path = require("path")

const EsmWebpackPlugin = require("@purtuga/esm-webpack-plugin")

let config = {
  entry: "./client/video-watch-client-plugin.js",
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "./video-watch-client-plugin.js",
    library: "script",
    libraryTarget: "var"
  },
  plugins: [
    new EsmWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /videojs.ima.js$/,
        use: [
          {
            loader: 'imports-loader',
            options: {
              define: ">false",
              exports: ">false",
              holder: ">{name:null, init:null}",
              this: ">{videojs:{registerPlugin:(name,init) => {holder.name = name; holder.init = init;}}}",
            }
          },
          {loader: 'exports-loader', options: "holder"},
        ]
      },
    ]
  }
}

module.exports = config
